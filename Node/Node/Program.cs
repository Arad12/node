﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Node
{
    class Program
    {
        public static Node<int> EXer(int amount,int lim1, int endlim)
        {
            // this function returns a random int Node between two parameters.
            Random rand = new Random();
            Node<int> a = new Node<int>(rand.Next(lim1, endlim));
            Node<int> p = a; // why is it points on the last sit?
            for (int i =0; i< amount; i++)
            {
                int num = rand.Next(lim1, endlim);
                p.SetNext(new Node<int>(num));
                p = p.GetNext();
            }
            return a;
        }

        public static Node<int> eee()
        {
            // in this function the user build a Node 
            Console.WriteLine("enter a number into the Node");
            int num = int.Parse(Console.ReadLine());
            Node<int> first = new Node<int>(num);
            Node<int> last = first;
            while(num != -999)
            {
                Console.WriteLine("enter a number into the Node");
                num = int.Parse(Console.ReadLine());
                last.SetNext(new Node<int>(num));
                last = last.GetNext();

            }

            return first;
        }

        public static void page67(Node<int> aa, int num)
        {
            // this function gets a list and a number. for each number at the node, ill be added another number whick complete their sum to the number
            // למה לא הייתי צריך להשתמש פה ב 
            // ref
            // וזה בכל זאת משנה את הרשימה המקורית
            Node<int> last = aa;
            while (last!=null)
            {
                Node<int> a = new Node<int>();
                a.SetValue(num - last.GetValue());
                a.SetNext(last.GetNext());
                last.SetNext(a);
                last = last.GetNext().GetNext();
            }
        }

        public static void canceldouble(Node<int> list)
        {
            // this function cancel cuple of even numbers
            Node<int> copy = list;
            while(copy.HasNext())
            {
                if (copy.GetValue() == copy.GetNext().GetValue())
                    copy.SetNext(copy.GetNext().GetNext());
                else
                    copy = copy.GetNext();
            }
        }

        public static void deletenum(Node<int> node, int num)
        {
            // this function gets a number and a list. it delete the chossen number from the list.
            Node<int> copy = node;
            while (copy.GetNext() != null)
            {
                if (copy.GetNext().GetValue() == num)
                {
                    copy.SetNext(copy.GetNext().GetNext());
                }
                else
                copy = copy.GetNext();
            }
            if (node.GetValue() == num)
                node = node.GetNext();
        }

        public static int howlong(Node<int> node)
        {
            // this function check the length of the node
            int counter = 0;
            Node<int> copy = node;
            while(copy != null)
            {
                counter++;
                copy = copy.GetNext();
            }
            return counter;
        }

        public static bool doestheyeven(Node<int> node)
        {
            // this function check if each part is even to the other
            int howlong1 = howlong(node);
            Node<int> copy1 = node;
            Node<int> copy2 = node;
            Node<int> copy3 = node;
            for (int i = 0; i<howlong1 / 3; i++)
            {
                copy2 = copy2.GetNext();
                copy3 = copy3.GetNext().GetNext();
            }
            for(int i =0; i<howlong1/3; i++)
            {
                if ((copy1.GetValue() != copy2.GetValue()) || (copy3.GetValue() != copy2.GetValue()) || (copy3.GetValue() != copy1.GetValue()))
                    {
                    return false;
                    }
                copy1 = copy1.GetNext();
                copy2 = copy2.GetNext();
                copy3 = copy3.GetNext();
            }
            
            return true;
        }

        public static bool thirdlist(Node<int> node)
        {
            // if the list is divides in 3 and each part is even to the others, the answer is true
            if (howlong(node) % 3 == 0)
            {
                if (doestheyeven(node))
                    return true;
            }
            return false;
        }
        
        public static void Insertinsortway (Node<int> node, int num)
        {
            // this function gets a number and a list. it enters the number into the list and save the list organized.

            Node<int> copy = node;
            if (copy.GetValue() > num)
            {
                Node<int> ne = new Node<int>(num);
                ne.SetNext(copy);
                node = ne;
            }
            else
            {
                while (copy != null)
                {
                    if (copy.GetNext() == null)
                    {
                        copy.SetNext(new Node<int>(num));
                        break;
                    }
                    else if (copy.GetNext().GetValue() < num)
                        copy = copy.GetNext();
                    else 
                     {
                        Node<int> newone = new Node<int>(num);
                        newone.SetNext(copy.GetNext());
                        copy.SetNext(newone);
                        break;
                    }

                }
            }
        }

        public static void Insertionsort(Node<int> node)
        {
            // this function gets a list.
            // the user insert numbers as he want to and it added to the list.
            int answer = 0;
            while(answer != -1)
            {
                Console.WriteLine("enter a number into the list");
                answer = int.Parse(Console.ReadLine());
                Insertinsortway(node, answer);
                Console.WriteLine(node.ToString());
            }
        }

        public static Node<int> mergelists(Node<int> first, Node<int> sec)
        {
            //this function gets two lists and merge them together to one sort list
            Node<int> firstcopy = first;
            Node<int> seccopy = sec;
            Node<int> final = first;
            int howlong2 = howlong(seccopy);
            for (int i=0; i< howlong2; i++ )
            {
                Insertinsortway(final, seccopy.GetValue());
                seccopy = seccopy.GetNext();
            }
            return final;
        }

        static void Main(string[] args)
        {
            
        }
    }

    public class Node<T>
    {
        private T value;
        private Node<T> next;

        public Node(T value)
        {
            this.value = value;
            this.next = null;
        }

        public Node(T value, Node<T> next)
        {
            this.value = value;
            this.next = next;
        }

        public Node()
        {

        }

        public T GetValue()
        {
            return this.value;
        }

        public Node<T> GetNext()
        {
            return this.next;
        }

        public bool HasNext()
        {
            return (this.next != null);
        }

        public void SetValue(T value)
        {
            this.value = value;
        }

        public void SetNext(Node<T> next)
        {
            this.next = next;
        }

        public override string ToString()
        {
            return this.value + "-->" + this.next;
        }


        /*
        public int howlong()
        {
            int counter = 0;
            while(this.value != null)
            {
                counter++;
                this.valu = this.next.value;
            }
            return counter;
        }
        */
    }
}
